import { IsNotEmpty, IsNumber } from 'class-validator';

class CreateOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  @IsNumber()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
}
